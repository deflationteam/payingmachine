﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using payingMachine.Controller;
using payingMachine.Repository;

namespace payingMachine.Views.Pages
{
    /// <summary>
    /// Interaction logic for end.xaml
    /// </summary>
    public partial class end : Page
    {
        public end()
        {
            InitializeComponent();

            var ticket = TicketController.Instance.GetTicketString();
            BarCodeRepository.Generate(ticket);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Welcome());
        }
    }
}
