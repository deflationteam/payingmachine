﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace payingMachine.Exceptions
{
    class InvalidChecksumException : Exception
    {
        public InvalidChecksumException() : base() { }
    }
}
