﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace payingMachine.Exceptions
{
    class InvalidTimestampException : Exception
    {
        public InvalidTimestampException() : base() { }
    }
}
