﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using payingMachine.Exceptions;
using payingMachine.Repository;

namespace payingMachine.Controller
{
    public class TicketController : Controller<TicketController>
    {
        private byte[] _IV = new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        public void ReadTicket()
        {
            var encodedBase64 = ScanCode();
            LoadTicket(encodedBase64);

            try
            {
                Verify();
            }
            catch (InvalidTimestampException)
            {
                // Printing error message, given timestamp was invalid
            }
            catch (InvalidChecksumException)
            {
                // Printing error message, given checksum was invalid
            }
        }

        private struct Ticket
        {
            public long timestamp;
            public string checksum;
            public bool valid;
        }

        private Ticket TicketInformation = new Ticket()
        {
            valid = false
        };

        private void LoadTicket(string encodedBase64)
        {
            byte[] data = Convert.FromBase64String(encodedBase64);
            var decodedString = Encoding.UTF8.GetString(data);

            try
            {
                var decryptedString = CryptoRepository.XorBytes(_IV, Encoding.ASCII.GetBytes(decodedString));

                TicketInformation.timestamp = Convert.ToInt64(decryptedString.Substring(0, 10));
                TicketInformation.checksum = decryptedString.Substring(10, 6);
            }
            catch (ArgumentException)
            {
                /* Given strings arent same size */
            }
            catch (FormatException)
            {
                /* Given string after decrypt wasnt a correct input */
            }
        }

        public string GetTicketString()
        {
            if (TicketInformation.valid == false) 
                throw new NoTicketFoundException();

            var ts = CurrentTimestamp;
            var result = ts + CryptoRepository.GenerateCuttedMD5(ts, 6);

            return CryptoRepository.XorBytes(Encoding.ASCII.GetBytes(result), _IV);
        }

        private string ScanCode()
        {
            return BarCodeRepository.Scan();
        }

        private void Verify()
        {
            var hash = CryptoRepository.GenerateCuttedMD5(TicketInformation.timestamp.ToString(), 6);

            if (hash.ToLower() != TicketInformation.checksum)
            {
                TicketInformation.valid = false;
                throw new InvalidChecksumException();
            }

            TicketInformation.valid = true;
        }

        private DateTime GetTimestamp
        {
            get
            {
                try
                {
                    var dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                    dt = dt.AddSeconds(TicketInformation.timestamp).ToLocalTime();

                    return dt;
                }
                catch (Exception)
                {
                    throw new NoTicketFoundException();
                }
            }
        }

        private string CurrentTimestamp
        {
            get
            {
                return ((int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
            }
        }
    }
}
