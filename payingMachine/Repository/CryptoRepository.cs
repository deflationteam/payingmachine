﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace payingMachine.Repository
{
    public static class CryptoRepository
    {
        private static MD5 _md5 = System.Security.Cryptography.MD5.Create();

        public static string XorBytes(byte[] a, byte[] b)
        {
            if (a.Count() != b.Count()) throw new ArgumentException();

            var result = "";

            for (int i = 0; i < a.Length; i++)
            {
                result += (char)(a[i] ^ b[i]);
            }

            return result;
        }

        public static string GenerateCuttedMD5(string input, int len)
        {
            var inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            var hash = _md5.ComputeHash(inputBytes);

            var sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString().Substring(0, 6);
        }
    }
}
